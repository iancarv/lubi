import dj_database_url

from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES['default'] = dj_database_url.config()
ALLOWED_HOSTS = [".herokuapp.com", ]

BASE_URL = 'http://lubi-bot.herokuapp.com/'