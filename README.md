# Lubi

## Apresentação

Chatbot desenvolvido para a entrevista no LuizaLabs.
O Chatbot é restrito ao domínio das categorias de produtos comercializados pela Magazine Luiza, sendo capaz de entender trocas de contexto e utiliza Processamento de Linguagem Natural para definir a intenção do usuário

## Configuração

### Base de dados

Para configurar a base de dados, as seguintes variáveis de ambiente são utilizadas:

* DB_NAME
* DB_USER
* DB_HOST
* DB_PORT

### Facebook

* FB_URL_PATTERN: Padrão de url para receber as chamadas do Facebook. Os requests serão enviados para domain.com/fb_lubi/<FB_URL_PATTERN>.
* FB_VERIFY_TOKEN: Token de verificação do Facebook. Deve ser customizado ao associar o bot com um aplicativo do Facebook.
* FB_PAGE_TOKEN: Token da página do Facebook, você precisa dele para interagir programaticamente com uma página do Facebook.

Mais detalhes sobre como configurar um aplicativo para interagir com uma página através do Messenger utilizando o [link](https://developers.facebook.com/docs/messenger-platform/getting-started/quick-start)

## Criando a imagem

Para gerar a imagem docker, basta rodar o comando 

```bash
docker-compose build
```

## Treinando o modelo

A pasta fb_lubi/ml_chat/model possui um modelo previamente testado, porém retreiná-lo à partir de um novo dataset é realmente simples.
O dataset para treinamento está localizado em fb_lubi/ml_chat/data. Nele é possível adicionar novas intenções através de padrões que o programa deve reconhecer e respostas das quais ele deve selecionar.

Após alterar o dataset, para retreinar os dados basta rodar:

```bash
docker-compose run web python3 fb_lubi/ml_chat/learn.py 
```


## Rodando o chatbot

O chatbot pode ser rodado utilizando um container Docker. Para subir um container rode

```bash
docker-compose up
```

O Chatbot ficará disponível à partir da URI localhost:8000


## Testando

Para rodar os testes, à partir da raiz do projeto rode:

```bash
docker-compose run web python3 -m unittest tests/test_ml.py
```