import unittest
import logging

from fb_lubi.ml_chat.talk import response, intents, context
from fb_lubi.ml_chat.strategies import order_strategy, offer_strategy


class MLTestCase(unittest.TestCase):

    def setUp(self):
        self.user_id = 'ianc'

    def test_option_name(self):
        all_options = [m for m in intents['intents'] if m.get('option_name')]
        for item in all_options:
            context[self.user_id] = item.get('context_filter', '')
            r, tag = response(item['option_name'], self.user_id)
            self.assertEqual(tag, item['tag'])


class StrategiesTestCase(unittest.TestCase):

    def setUp(self):
        self.user_id = 'ianc'

    def test_offer(self):
        cases = ['cp', 'tf', 'pi', 'mo', ]
        for case in cases:
            offer_msg = offer_strategy(case, 'Msg')
            self.assertTrue('&categoria={}'.format(case) in offer_msg)

    def test_order(self):
        all_status = ['O pedido está sendo processado',
                      'Pedido já entregue',
                      'Pedido inexistente',
                      'Pedido em transito']
        status = order_strategy(3467645)
        self.assertTrue(status in all_status)

if __name__ == '__main__':
    unittest.main()
