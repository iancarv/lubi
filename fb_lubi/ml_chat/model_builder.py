import tflearn

import os

base_path = os.path.dirname(__file__)
if base_path == '':
    base_path = '.'


def build_model(len_x, len_y):
	net = tflearn.input_data(shape=[None, len_x])
	net = tflearn.fully_connected(net, 16)
	net = tflearn.fully_connected(net, 16)
	net = tflearn.fully_connected(net, len_y, activation='softmax')
	net = tflearn.regression(net)

	# Define model and setup tensorboard
	model = tflearn.DNN(net, tensorboard_dir=base_path + 'logs/tflearn_logs')
	return model