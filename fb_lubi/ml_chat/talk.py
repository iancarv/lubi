import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()

import numpy as np
import tflearn
import tensorflow as tf
import random

import os
import sys

from .strategies import offer_strategy, order_strategy

from .model_builder import build_model
from .nlp_tools import bow

base_path = os.path.dirname(__file__)
if base_path == '':
    base_path = '.'
nltk.data.path.append(base_path)

import pickle
data = pickle.load(open(base_path + "/model/training_data", "rb"))
words = data['words']
classes = data['classes']
train_x = data['train_x']
train_y = data['train_y']

import json
with open(base_path + '/data/intents.json') as json_data:
    intents = json.load(json_data)

model = build_model(len(train_x[0]), len(train_y[0]))
model.load(base_path + '/model/model.tflearn')


strategy_lookup = {
    'offer_strategy': offer_strategy
}

context = {}

ERROR_THRESHOLD = 0.25


def classify(sentence):
    results = model.predict([bow(sentence, words)])[0]
    results = [[i, r] for i, r in enumerate(results) if r > ERROR_THRESHOLD]
    results.sort(key=lambda x: x[1], reverse=True)

    return_list = []
    for r in results:
        return_list.append((classes[r[0]], r[1]))

    return return_list

def response(sentence, user_id='123', show_details=False):
    if user_id in context and context[user_id] == 'pedido':
        return order_strategy(sentence)

    results = classify(sentence)

    if results:
        while results:
            for i in intents['intents']:
                if i['tag'] == results[0][0]:
                    if 'context_set' in i:
                        if show_details:
                            print('context:', i['context_set'])
                        context[user_id] = i['context_set']
                    if not 'context_filter' in i or \
                            (user_id in context and 'context_filter' in i and i['context_filter'] == context[user_id]):
                        if show_details:
                            print('tag:', i['tag'])
                        response = random.choice(i['responses'])
                        if not isinstance(response, str):
                            strategy = response['strategy']
                            fn = strategy_lookup[strategy]
                            response = fn(i['tag'], sentence)

                        return (response, i['tag'])

            results.pop(0)

    all_options = [m for m in intents['intents'] if m.get('option_name')]
    options = []

    ctx = context.get(user_id)
    if ctx:
        options = [m['option_name']
                   for m in all_options if m.get('context_filter') == ctx]

    options = [m['option_name']
               for m in all_options if m.get('context_filter') is None]

    # TODO: melhor experiência para o usuário
    response = '\n'.join(options)
    return ('Essas são as opções disponíveis:\n' + response, 'nada')
