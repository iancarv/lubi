import nltk
from nltk.stem.lancaster import LancasterStemmer
import numpy as np
import tflearn
import tensorflow as tf
import random
from model_builder import build_model
from nlp_tools import remove_accents
import json
import os

base_path = os.path.dirname(__file__)
if base_path == '':
    base_path = '.'

nltk.data.path.append(base_path)
stemmer = nltk.stem.RSLPStemmer()

with open(base_path + '/data/intents.json') as json_data:
    intents = json.load(json_data)


words = []
classes = []
documents = []
ignore_words = ['?']
for intent in intents['intents']:
    for pattern in intent['patterns']:
        w = nltk.word_tokenize(pattern)
        words.extend(w)
        documents.append((w, intent['tag']))

        if intent['tag'] not in classes:
            classes.append(intent['tag'])

words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
words = sorted(list(set(words)))

classes = sorted(list(set(classes)))

print(len(documents), 'documents')
print(len(classes), 'classes', classes)
print(len(words), 'unique stemmed words', words)

training = []
output = []
output_empty = [0] * len(classes)

for doc in documents:
    bag = []
    pattern_words = doc[0]
    pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]
    for w in words:
        bag.append(1) if w in pattern_words else bag.append(0)

    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1

    training.append([bag, output_row])

random.shuffle(training)
training = np.array(training)
train_x = list(training[:, 0])
train_y = list(training[:, 1])
tf.reset_default_graph()
model = build_model(len(train_x[0]), len(train_y[0]))
model.fit(train_x, train_y, n_epoch=1000, batch_size=16, show_metric=True)
model.save(base_path + '/model/model.tflearn')

import pickle
pickle.dump({'words': words, 'classes': classes, 'train_x': train_x,
             'train_y': train_y}, open(base_path + '/model/training_data', 'wb'))
