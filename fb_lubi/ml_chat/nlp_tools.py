import nltk
import numpy as np
import unicodedata
import os

base_path = os.path.dirname(__file__)
if base_path == '':
    base_path = '.'
nltk.data.path.append(base_path)

tokenizer = nltk.data.load(base_path + '/tokenizers/punkt/portuguese.pickle')
stemmer = nltk.stem.RSLPStemmer()
def tokenize(sent):
	return nltk.word_tokenize(sent)

def stem_words(sentence_words):
	return [stemmer.stem(word.lower()) for word in sentence_words]

def clean_up_sentence(sentence):
    # tokenize the pattern
    sentence_words = tokenize(sentence)
    # stem each word
    sentence_words = stem_words(sentence_words)
    return sentence_words

def bow(sentence, words, show_details=False):
    # tokenize the pattern
    sentence_words = clean_up_sentence(sentence)
    print(sentence_words)
    # bag of words
    bag = [0]*len(words)  
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s: 
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)

    return(np.array(bag))

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii

