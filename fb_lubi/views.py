from django.conf import settings
from django.shortcuts import render
from django.views import generic
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from django.conf import settings
import json
import requests
from pprint import pprint


from .ml_chat.talk import response
# Create your views here.


class LubiView(generic.View):

    def get(self, request, *args, **kwargs):
        token = settings.FB['VERIFY_TOKEN']
        if self.request.GET['hub.verify_token'] == token:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    def post(self, request, *args, **kwargs):
        # Converts the text payload into a python dictionary
        incoming_message = json.loads(self.request.body.decode('utf-8'))
        # Facebook recommends going through every entry since they might send
        # multiple messages in a single call during high load
        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                # Check to make sure the received call is a message call
                # This might be delivery, optin, postback for other events
                if 'message' in message:
                    # Print the message to the terminal
                    fbid = message['sender']['id']
                    received_message = message['message']['text']
                    r, tag = response(received_message, fbid)
                    post_facebook_message(fbid, r)     
        
        return HttpResponse(status=200)


def post_facebook_message(fbid, received_message):           
    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=EAAB4tQggRCsBAEBf0CXxiKZBClaJkXCZChPXYsXROHsWgtwGiDlNILxsAR8feML0YI7YfBBqtBrKwZBA81EIJkzGBmPtl5amXVsIfkzZCRi90dHsDPuRbAtInQBK9yZB4RCFu4YpyTgz2W4ZCULjcZA3m6p78ZAHtwoZB3yxAj0U9jgZDZD' 
    response_msg = json.dumps({"recipient":{"id":fbid}, "message":{"text":received_message}})
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)
    pprint(status.json())
