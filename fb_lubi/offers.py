def offers(sources='website', **kwargs):
    lookup = {
        'website': offers_from_site
    }

    fn = lookup.get(sources, offers_from_site)

    offers = fn(**kwargs)

    return offers


def offers_from_site(**kwargs):
    itens = kwargs.get('itens', 60)
    categoria = kwargs.get('categoria')

    response = 'Você pode olhar a oferta através do site\n' \
        'http://especiais.magazineluiza.com.br/saldao/'

    if categoria is not None or categoria == 'todos':
        response += '/?tipo=grande&itens={}' \
            '&categoria={}'.format(itens, categoria)

    response += '\nGostaria de verificar mais alguma promoção?'
    return response
