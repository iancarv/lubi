from django.conf import settings
from django.shortcuts import render
from django.views import generic
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
import json
import requests
from pprint import pprint
from .ml_chat.talk import response


class ChatView(generic.View):
    def get(self, request, *args, **kwargs):
        return HttpResponse('Ok', status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    def post(self, request, *args, **kwargs):
        payload = json.loads(self.request.body.decode('utf-8'))
        user_id = payload['user_id']
        message = payload['message']
        r = response(message, user_id)
        

        return HttpResponse(r, status=200)
