from django.apps import AppConfig


class ShippingStatusConfig(AppConfig):
    name = 'shipping_status'
