from django.conf import settings
from django.shortcuts import render
from django.views import generic
from django.http.response import HttpResponse


class ShippingView(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['order']:
        	# TODO: Integrar com backend
            return HttpResponse('Seu pedido ainda esta sendo processado')
        else:
            return HttpResponse('Erro, por favor envie um codigo de pedido valido')

    